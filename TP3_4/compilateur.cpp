//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <map>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {UNSIGNED_INT, BOOL, CHAR, FLOAT};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, TYPE> DeclaredVariables;
//set<string> DeclaredVariables;
unsigned long TagNumber=0;
unsigned long CondNumber=0;
unsigned long BouwNumber=0;
unsigned long BoufNumber=0;
unsigned long BlocNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

string TypeToString(TYPE T){
	string t;
	if(T == UNSIGNED_INT){
		t = "unsigned_int";
	}
	else if(T == BOOL){
		t = "boolean";
	}
	else if(T == CHAR){
		t = "character";
	}
	else if(T == FLOAT){
		t = "floatant";
	}
	else{
		t = "inconnu";
	}
	return t;
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	TYPE type = UNSIGNED_INT;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	TYPE N = UNSIGNED_INT;
	return N;
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE F;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		F = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			F = Number();
		else
			if(current==ID)
				F = Identifier();
			else
				Error("'(' ou chiffre ou lettre attendue");
	return F;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	OPMUL mulop;
	TYPE T, S;
	T = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		S = Factor();
		string t = TypeToString(T);
		string s = TypeToString(S);
		if(S!=T)
			Error("Les types ne correspondent pas : le type de T est "+t+" et le type de S est "+s);
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return T;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE SE, V;
	SE = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		V = Term();
		string se = TypeToString(SE);
		string v = TypeToString(V);
		if(V!=SE)
			Error("Les types ne correspondent pas : le type de SE est "+se+" et le type de V est "+v);
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return SE;
}

void VarDeclarationPart(void){
	if(current!=VAR)
		Error("caractère 'VAR' attendu");
	current=(TOKEN) lexer->yylex();
	DeclarationPart();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		DeclarationPart();
	}
	current=(TOKEN) lexer->yylex();
}

// DeclarationPart := "[" Ident {"," Ident} "]"
TYPE DeclarationPart(void){
	TYPE var;
	cout << "\t.data"<<endl;
	cout << "FormatString1:\t.string \"le resultat est %llu\\n\""<<endl;	// used by printf to display 64-bit unsigned interers
	cout << "\t.align 8"<<endl;
	if(current!=ID)
		Error("Un identificater était attendu");
	vector v = new vector();
	v.push_back(lexer->YYText());
	DeclaredVariables.insert(pair<string, TYPE>(lexer->YYText(), UNSIGNED_INT));
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		v.push_back(mexer->YYText());
		DeclaredVariables.insert(pair<string, TYPE>(lexer->YYText(), UNSIGNED_INT));
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	if(current==BOOLEAN){
		for(int i = 0; i < v.size(); i++){
			DeclaredVariables.insert(pair<string, TYPE>(v[i], BOOL));
			cout << lexer->YYText() << ":\t.quad 0"<<endl;
		}
	}
	else if(current==INTEGER){
		for(int i = 0; i < v.size(); i++){
			DeclaredVariables.insert(pair<string, TYPE>(v[i], UNSIGNED_INT));
			cout << lexer->YYText() << ":\t.quad 0"<<endl;
		}
	}
	else if(current==CHARACTER){
		for(int i = 0; i < v.size(); i++){
			DeclaredVariables.insert(pair<string, TYPE>(v[i], CHAR));
			cout << lexer->YYText() << ":\t.byte 0"<<endl;
		}
	}
	else if(current==FLOATANT){
		for(int i = 0; i < v.size(); i++){
			DeclaredVariables.insert(pair<string, TYPE>(v[i], FLOAT));
			cout << lexer->YYText() << ":\t.double 0.0"<<endl;
		}
	}
	else{
		Error("ce type n'est pas accepté");
	}
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	OPREL oprel;
	TYPE E, M;
	E = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		M = SimpleExpression();
		string e = TypeToString(E);
		string m = TypeToString(M);
		if(M!=E)
			Error("Les tyes ne correspondent pas : le type de E est "+e+" et le type de M est "+m);
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOL;
	}
	return E;
}

// AssignementStatement := Identifier ":=" Expression
TYPE AssignementStatement(void){
	TYPE A, var;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	var = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	A = Expression();
	string a = TypeToString(A);
	string vr = TypeToString(var);
	if(A != var)
		Error("les types ne correspondent pas : le type de A est "+a+" et le type de var est "+vr);
	cout << "\tpop "<<variable<<endl;
}

void Statement(void);

void Display(void){
	TYPE D;
	current=(TOKEN) lexer->yylex();
	D = Expression();
	if(D==UNSIGNED_INT){
		cout << "\tpop %rdx"<< endl;					// The value to be displayed
		cout << "\tmovq $FormatString1, %rsi"<< endl;	// "%llu\n"
		cout << "\tmovl $1, %edi"<< endl;
		cout << "\tmovl $0, %eax"<< endl;
		cout << "\tcall __printf_chk@PLT"<< endl;
	}
	else{
		if(D==BOOL){
			cout << "\tpop %rdx"<<e ndl;
			cout << "\tcompq $0, %rdx"<< endl;
			cout << "\tje Faux" << endl;
			cout << "\tmovq $TrueString, %rsi\t# True" << endl;
			cout << "\tjmp Suite"<<++TagNumber<< endl;
			cout << "Faux"<<TagNumber<<":";
			cout << "\tmovq $FalseString, %rsi\t# False"<< endl;
			cout << "Suit"<<TagNumber<<":";
			cout << "\tmovl $1, %edi"<< endl;
			cout << "\tmovl $0, %eax"<< endl;
			cout << "\tcall __printf_chk@PLT"<< endl;
		}
		else{
			Error("DISPLAY ne peut pas afficher la valeur");
		}
	}
}

void IfStatement(void){
	TYPE I;
	if(current!=IF)
		Error("Condition if attendue");
	current=(TOKEN) lexer->yylex();
	cout << "If"<<++CondNumber<<":";
	I = Expression();
	if(I != BOOL)
		Error("ce n'est pas un booléen");
	cout << "\tpop %rax"<<endl;
	cout <<"\tjz Else"<<CondNumber<<endl;
	if(current!=THEN)
		Error("Condition then attendue");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout <<"\tjmp Suitei"<<CondNumber<<endl;
	if(current==ELSE)
		current=(TOKEN) lexer->yylex();
		cout <<"Else"<<CondNumber<<":";
		Statement();
		cout <<"Suitei"<<CondNumber<<":"<<endl;
}

void WhileStatement(void){
	TYPE W;
	if(current!=WHILE)
		Error("Boucle while attendue");
	current=(TOKEN) lexer->yylex();
	++BouwNumber;
	cout << "While"<<BouwNumber<<":";
	W = Expression();
	if(W != BOOL)
		Error("ce n'est pas un booléen");
	cout << "\tpop %rax"<<endl;
	cout <<"\tjz Suitew"<<BouwNumber<<endl;
	if(current!=DO)
		Error("Instruction attendue");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout <<"\tjmp While"<<BouwNumber<<endl;
	cout <<"Suitew"<<BouwNumber<<":"<<endl;
}

void ForStatement(void){
	TYPE BF;
	if(current!=FOR)
		Error("Boucle for attendue");
	current=(TOKEN) lexer->yylex();
	++BoufNumber;
	AssignementStatement();
	cout <<"For"<<BoufNumber<<":";
	if(current!=TO)
		Error("Expression attendue");
	current=(TOKEN) lexer->yylex();
	BF = Expression();
	if(BF != BOOL)
		Error("Expression booléenne attendue");
	cout <<"\tpop %rax"<<endl;
	cout <<"\tjz Suitef"<<BoufNumber<<endl;
	if(current!=DO)
		Error("Instruction attendue");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout <<"\taddq $1, i"<<endl;
	cout <<"\tjmp For"<<BoufNumber<<endl;
	cout <<"Suitef"<<BoufNumber<<":"<<endl;
}

void BlockStatement(void){
	if(current!=BEG)
		Error("Début du block attendu");
	current=(TOKEN) lexer->yylex();
	cout <<"Begin"<<++BlocNumber<<":";
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=END)
		Error("Fin du block attendu");
	current=(TOKEN) lexer->yylex();
	cout <<"End"<<BlocNumber<<":"<<endl;
}

// Statement := AssignementStatement
void Statement(void){
	if(current==ID)
		AssignementStatement();
	else if(current==IF)
		IfStatement();
	else if(current==WHILE)
		WhileStatement();
	else if(current==FOR)
		ForStatement();
	else if(current==BEG)
		BlockStatement();
	else if(current==DISP)
		Display();
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==VAR)
		VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}
}
		
			





